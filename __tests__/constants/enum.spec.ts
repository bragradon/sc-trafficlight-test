import {Direction, Colour} from "constants/enums";

/*
    Note that Typescript defines a reverse relationship
    for enums (key to value and value to key). Thus we
    need to divide its length by 2 to get the true count
    of how many entries it has.
 */

describe ('Traffic light designation spec', () => {
    test('2 traffic light designations', () => {
        expect(Object.keys(Direction).length / 2).toBe(2);
    });

    test('Designation 1 is NorthSouth', () => {
        expect(Direction.NorthSouth).toBeDefined();
    });

    test('Designation 2 is EastWest', () => {
        expect(Direction.EastWest).toBeDefined();
    });
});

describe('Traffic light colour spec', () => {
    test('3 Traffic light colours', () => {
       expect(Object.keys(Colour).length / 2).toBe(3);
    });

    test('Traffic light has red colour', () => {
        expect(Colour.Red).toBeDefined();
    });

    test('Traffic light has yellow colour', () => {
        expect(Colour.Yellow).toBeDefined();
    });

    test('Traffic light has green colour', () => {
        expect(Colour.Yellow).toBeDefined();
    });
});
