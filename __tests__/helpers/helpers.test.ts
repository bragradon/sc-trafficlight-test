import {LightState} from "constants/types";

beforeAll(() => jest.resetModules());

describe('waitingToEndIn Suite', () => {
    const {waitingToEndIn} = require('helpers/helpers');
    const start = new Date().getTime();

    test('yields true on first iteration', () => {
        let generator = waitingToEndIn(start, 10);
        let {value, done} = generator.next();

        expect(value).toBe(true);
        expect(done).toBe(false);
    });

    test('returns false on final iteration', () => {
        let generator = waitingToEndIn(start, 0);
        let counter = 0;

        for (let value of generator) {
            if (counter++ > 0) {
                expect(value).toBe(false);
            }
        }
    });

    test('throws error on negative start parameter', () => {
        let generator = waitingToEndIn(-1, 0);
        expect(() => generator.next()).toThrow()
    });

    test('throws error on negative seconds parameter', () => {
        let generator = waitingToEndIn(start, -1);
        expect(() => generator.next()).toThrow()
    });
});

describe('processLightChange Suite', () => {
    const {processLightChange} = require('helpers/helpers');

    beforeAll(() => {
        console.log = jest.fn();
    });

    function getLightState(delay=0, thenTransition=function(){}) {
        return <LightState> {
            delay,
            thenTransition,
            lights: [],
            simulationStartTime: 0
        };
    }

    test('returns promise', () => {
        let result = processLightChange(getLightState());
        expect(result).toBeInstanceOf(Promise);
    });

    test('runs transition function after delay', done => {
        processLightChange(getLightState(1, done));
    });
});
