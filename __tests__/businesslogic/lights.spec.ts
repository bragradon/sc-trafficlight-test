import {lights} from "businesslogic/lights";
import {Light} from "constants/types";

describe ("Traffic light simulation 'lights' spec", () => {
    test('2 traffic light are defined', () => {
        expect(lights.length).toBe(2);
    });

    test('2 different traffic lights are defined', () => {
        let [light1, light2]: Light = lights;
        expect(light1.direction).not.toBe(light2.direction);
    });

    test('The traffic lights have different stating colours', () => {
        let [light1, light2]: Light = lights;
        expect(light1.colour).not.toBe(light2.colour);
    });
});
