import {Light} from "constants/types";
import {Colour, Direction} from "constants/enums";
import * as helpers from "helpers/helpers";

describe('Traffic Light Simulation Spec Suite', () => {
    jest.mock('../../src/helpers/helpers', () => {
        return {
            processLightChange: jest.fn(),
            waitingToEndIn: jest.fn(),
        };
    });

    const {simulate} = require("businesslogic/trafficLightSimulator");
    const {processLightChange, waitingToEndIn} = require('../../src/helpers/helpers');
    const consoleLogMock = jest.fn();

    beforeEach(() => {
        jest.resetAllMocks();
        console.log = consoleLogMock;
    });

    function runSimulationForNLoops(n: number) {
        waitingToEndIn.mockReturnValue({
            [Symbol.iterator]: function* () {
                for (let i=0; i < n; i++) {
                    yield true;
                }
                return false;
            }
        });
    }

    function processLightsWithNoDelay() {
        processLightChange.mockImplementation((args) => {
            args.delay = 0;
            return helpers.processLightChange(args);
        });
    }

    test('Simulation runs for 30 minutes', () => {
        runSimulationForNLoops(0);
        simulate();
        expect(waitingToEndIn).toBeCalledWith(expect.anything(), 30 * 60);
    });

    test('Lights change every 5 minutes', done => {
        runSimulationForNLoops(1);
        simulate().then(() => {
            let [[{delay}]] = processLightChange.mock.calls;
            expect(delay).toBe(5 * 60);
            done();
        });
    });

    test('Yellow light displays for 30 seconds', done => {
        runSimulationForNLoops(1);
        simulate().then(() => {
            let [, [{delay}]] = processLightChange.mock.calls;
            expect(delay).toBe(30);
            done();
        });
    });

    test('Output is provided for each light transition', done => {
        consoleLogMock.mockReset();
        runSimulationForNLoops(1);
        processLightsWithNoDelay();

        simulate().then(() => {
            expect(consoleLogMock).toBeCalled();
            let [[output1], [output2],] = consoleLogMock.mock.calls;

            expect(output1).toContain("Red");
            expect(output1).toContain("Green");

            expect(output2).toContain("Red");
            expect(output2).toContain("Yellow");

            done();
        });
    });

    test('Green light transitions to Yellow', done => {
        runSimulationForNLoops(1);

        let lights: Light[] = [
            {direction: Direction.NorthSouth, colour: Colour.Red},
            {direction: Direction.EastWest, colour: Colour.Green},
        ];

        simulate(lights).then(() => {
            let [[{thenTransition}]] = processLightChange.mock.calls;
            thenTransition();

            let [light0, light1] = lights;

            expect(light0.colour).toBe(Colour.Red);
            expect(light1.colour).toBe(Colour.Yellow);
            done();
        });
    });

    test('Lights switch Red/Green colours', done => {
        runSimulationForNLoops(1);

        let lights: Light[] = [
            {direction: Direction.NorthSouth, colour: Colour.Red},
            {direction: Direction.EastWest, colour: Colour.Green},
        ];

        simulate(lights).then(() => {
            let [, [{thenTransition}]] = processLightChange.mock.calls;
            thenTransition();

            let [light0, light1] = lights;

            expect(light0.colour).toBe(Colour.Green);
            expect(light1.colour).toBe(Colour.Red);
            done();
        });
    });
});
