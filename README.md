# README for solution to SafetyCulture Traffic Light test

## How to run solution

### Quick and Easy approach
The quickest method to see the code running is to open /dist/index.html in a web browser

### Build steps
This allows for building from the source code and allows for
* Running tests
* Running as a cli program
* Running in web browser

#### As follows:
* Install Node.js version 7+
* Install Yarn [instructions](https://yarnpkg.com/en/docs/install)
* Install dependencies: ```yarn install```
* To run tests: ```yarn test```
* Build and run project as Node cli program: ```yarn cli```
* Build project for running in web browser: ```yarn build:browser```
    * The files will be in the dist/ folder ready for deployment
    * You can just open the index.html in a browser
* Build and run project in web browser: ```yarn browser```

## Milestone points
* [Initial single file node cli solution](https://bitbucket.org/bragradon/sc-trafficlight-test/commits/a03296b9bc42eec7a68dabde5ee3b59ef1c4726d)
* [Split into modules with tests on cli](https://bitbucket.org/bragradon/sc-trafficlight-test/commits/e7766c4497fc93dd294d659932844d0b82363eae)
* [Bundled project into web browser interface](https://bitbucket.org/bragradon/sc-trafficlight-test/commits/d8b8a45813b545885fece42cbd104c9d3c2d75cd)

## Notes
I have written the solution to demonstrate the following:

* Familiarity with the in [Typescript](https://www.typescriptlang.org/) language
    * Use of ES.next functionality
    * Use of asynchronous programming
    * Use of generator functions
    * Type declarations
    * It should be noted that this is similar to [Python 3](https://docs.python.org/3/library/asyncio-task.html)
* Demonstrate Project structure
    * Setup of Node backend structure with [Yarn](https://yarnpkg.com/)
* UX/UI
    * Add progress bar when TTY available
    * [React.js](https://facebook.github.io/react/) web browser interface
        * Use of [Webpack](https://webpack.github.io/) to bundle project
        * Use of [Less.js](http://lesscss.org/)
        * Reuse of core logic from cli program
* Testing
    * Unit testing of helper functions
    * Specification testing for problem brief
    * Behavioural testing for main program
    * Coverage for source files