import {Colour, Direction} from "../constants/enums";
import {Light, LightState} from "../constants/types";

import {addProgressBar} from "./progressBar";

export function* waitingToEndIn(start: number, seconds: number) {
    if (start < 0) {
        throw new Error('start parameter must be positive')
    }
    if (seconds < 0) {
        throw new Error('seconds parameter must be positive')
    }

    let end = start;
    let delay = seconds * 1000;

    while(end < start + delay) {
        yield true;
        end = new Date().getTime();
    }

    return false;
}

export function processLightChange(state: LightState) {
    let {
        delay, thenTransition, lights, simulationStartTime
    } = state;

    delay *= 1000;
    logLightState(lights, simulationStartTime);

    return new Promise<void>(function(resolve) {
        let cleanup = addProgressBar(delay);
        setTimeout(() => {cleanup(); resolve(thenTransition()); }, delay);
    });
}

function logLightState(lights: Light[], start: number) {
    let currentElapsedTime = Math.round((new Date().getTime() - start) / 1000);
    let lightsOutput = '\n';

    for (let light of lights) {
        lightsOutput += `${Direction[light.direction]} is ${Colour[light.colour]}\n`;
    }

    console.log(`Timestamp: ${currentElapsedTime}${lightsOutput}`)
}
