import Gauge = require('gauge');
import isNode = require('detect-node');

export function addProgressBar(delay: number) {
    if (!isNode) {
        return () => {};
    }

    let progressBar = new Gauge(process.stderr, {
        updateInterval: 50,
    });
    let progress = 0;
    let end = new Date().getTime() + delay;

    let pulse = setInterval(() => progressBar.pulse(), 110);
    let updater = setInterval(function () {
        let now = new Date().getTime();
        let finish =  end - now;
        progress = (delay - finish) / delay;

        progressBar.show(`${Math.round(finish / 1000)} seconds until light transition`, progress);
    }, 100);
    progressBar.show();

    return () => {
        clearInterval(updater);
        clearInterval(pulse);
        progressBar.disable();
    };
}
