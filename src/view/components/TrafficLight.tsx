import * as React from 'react';
import * as classNames from 'classnames';
import * as style from './trafficlight.less';
import {Colour, Direction} from "../../constants/enums";

interface TrafficLightProps {
    direction: Direction,
    colour: Colour
}

export const TrafficLight: React.SFC<TrafficLightProps> = (props) => {
    const styles = classNames(style.lightwrapper, style[Direction[props.direction]]);
    return (
        <div className={styles}>
            <div className={style.trafficlight}>
                {
                    ['Red', 'Yellow', 'Green'].map(colour => {
                        let colourStyles = [style[colour]];
                        if (Colour[colour] == props.colour) {
                            colourStyles.push(style.on)
                        }

                        return (
                            <div
                                key={colour}
                                className={classNames(...colourStyles)}/>
                        );
                    })
                }
            </div>
        </div>
    );
};
