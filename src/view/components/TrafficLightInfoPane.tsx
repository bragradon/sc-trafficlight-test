import * as React from 'react';
import * as style from './trafficlight.less';
import {Light} from "../../constants/types";
import {Colour, Direction} from "../../constants/enums";

interface TrafficLightInfoPaneProps {
    timeUntilChange: number;
    lights: Light[];
}

export const TrafficLightInfoPane: React.SFC<TrafficLightInfoPaneProps> = (props) => {
    return (
        <div className={style.infoPane}>
            <table>
                <tbody>
                <tr>
                    <td>Time to next transition:</td>
                    <td>{props.timeUntilChange} {props.timeUntilChange === 1 ? 'second' : 'seconds'}</td>
                </tr>
                {
                    props.lights.map(light => {
                        return (
                            <tr key={light.direction}>
                                <td>{Direction[light.direction]} colour:</td>
                                <td>{Colour[light.colour]}</td>
                            </tr>
                        );
                    })
                }
                </tbody>
            </table>
        </div>

    );
};
