import * as React from 'react';
import * as style from './trafficlight.less';
import {TrafficLight} from "./TrafficLight";
import {TrafficLightInfoPane} from "./TrafficLightInfoPane";
import {Light, Transitions} from "../../constants/types";
import {Colour} from "../../constants/enums";
import {processLightChange} from "../../helpers/helpers";

export namespace App {
    export interface Props {
        lights: Light[];
        transitions: Transitions;
    }

    export interface State {
        delay: number;
        lights: Light[];
        simulationStartTime: number;
        currentGreenLight: number;
    }
}

export class App extends React.Component<App.Props, App.State> {
    timerID: number;

    constructor(props) {
        super(props);
        this.state = {
            delay: this.props.transitions.greenToYellow,
            lights: this.props.lights,
            simulationStartTime: new Date().getTime(),
            currentGreenLight: this.props.lights.findIndex(light => light.colour == Colour.Green)
        };
    }

    async changeGreenToYellow() {
        await processLightChange({
            delay: this.props.transitions.greenToYellow,
            thenTransition: () => {
                let {state} = this;
                let {lights, currentGreenLight} = state;
                lights[currentGreenLight].colour = Colour.Yellow;

                this.changeLightRedGreenDirection();
                this.setState({
                    ...state,
                    lights,
                    delay: this.props.transitions.yellowToRed,
                })
            },
            lights: this.state.lights,
            simulationStartTime: this.state.simulationStartTime,
        });
    }

    async changeLightRedGreenDirection() {
        await processLightChange({
            delay: this.props.transitions.yellowToRed,
            thenTransition: () => {
                let {state} = this;
                let {lights, currentGreenLight} = state;

                lights[currentGreenLight].colour = Colour.Red;
                currentGreenLight = (currentGreenLight + 1) % 2;
                lights[currentGreenLight].colour = Colour.Green;

                this.changeGreenToYellow();
                this.setState({
                    ...state,
                    lights,
                    currentGreenLight,
                    delay: this.props.transitions.greenToYellow,
                })
            },
            lights: this.state.lights,
            simulationStartTime: this.state.simulationStartTime,
        });
    }

    componentDidMount() {
        this.changeGreenToYellow();
        this.timerID = setInterval(() => this.setState({delay: this.state.delay - 1}), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    render() {
        const [light1, light2] = this.state.lights;

        return (
            <div className={style.container}>
                <div className={style.trafficLightPair}>
                    <div className={style.top}/>
                    <TrafficLight direction={light1.direction} colour={light1.colour} />
                    <TrafficLight direction={light2.direction} colour={light2.colour} />
                </div>
                <TrafficLightInfoPane
                    timeUntilChange={this.state.delay}
                    lights={this.state.lights}
                />
            </div>
        );
    }
}