import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { App } from './components/App';
import {lights, transitions} from '../trafficLightSimulator';

ReactDOM.render(<App lights={lights} transitions={transitions} />, document.getElementById('root'));
