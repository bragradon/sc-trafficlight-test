export enum Direction {
    NorthSouth,
    EastWest,
}

export enum Colour {
    Red,
    Yellow,
    Green,
}
