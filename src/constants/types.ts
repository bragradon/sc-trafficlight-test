import {Colour, Direction} from "./enums";

export type Light = {
    direction: Direction;
    colour: Colour;
}

export type LightState = {
    delay: number;
    thenTransition: Function;
    lights: Light[];
    simulationStartTime: number;
}

export type Transitions = {
    greenToYellow: number;
    yellowToRed: number;
}
