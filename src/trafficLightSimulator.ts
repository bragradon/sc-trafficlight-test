import {simulate} from "./businesslogic/trafficLightSimulator";
import {lights, transitions} from "./businesslogic/lights";
import {waitingToEndIn, processLightChange} from "./helpers/helpers";

export {
    simulate,
    lights,
    transitions,
    waitingToEndIn,
    processLightChange,
}
