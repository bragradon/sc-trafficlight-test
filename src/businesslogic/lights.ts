import {Colour, Direction} from "../constants/enums";
import {Light, Transitions} from "../constants/types";

export let lights: Light[] = [
    {direction: Direction.NorthSouth, colour: Colour.Red},
    {direction: Direction.EastWest, colour: Colour.Green},
];

export const transitions: Transitions = {
    greenToYellow: 5*60,
    yellowToRed: 30,
};
