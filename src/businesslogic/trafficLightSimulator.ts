import {Colour} from "../constants/enums";
import {Light, Transitions} from "../constants/types";
import {processLightChange, waitingToEndIn} from "../helpers/helpers";
import {lights as initialLights, transitions as initialTransitions} from "./lights";

export async function simulate(lights: Light[] = initialLights, transitions: Transitions = initialTransitions) {
    const simulationStartTime: number = new Date().getTime();
    let currentGreenLight: number = lights.findIndex(light => light.colour == Colour.Green);

    for (const processing of waitingToEndIn(simulationStartTime, 30 * 60)) {
        if (!processing) {
            break;
        }

        await processLightChange({
            delay: transitions.greenToYellow,
            thenTransition: () => lights[currentGreenLight].colour = Colour.Yellow,
            lights,
            simulationStartTime,
        });
        await processLightChange({
            delay: transitions.yellowToRed,
            thenTransition: () => {
                lights[currentGreenLight].colour = Colour.Red;
                currentGreenLight = (currentGreenLight + 1) % 2;
                lights[currentGreenLight].colour = Colour.Green;
            },
            lights,
            simulationStartTime,
        });
    }

    console.log("Done");
}
