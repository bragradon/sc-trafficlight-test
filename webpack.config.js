var webpack = require('webpack');
var path = require('path');

// variables
var isProduction = process.argv.indexOf('-p') >= 0;
var sourcePath = path.join(__dirname, './src');
var outPath = path.join(__dirname, './dist');

// plugins
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    cache: true,
    context: sourcePath,
    devtool: "source-map",

    entry: {
        polyfills: [
            'babel-polyfill'
        ],
        vendors: [
            'react',
            'react-dom'
        ],
        trafficlightsim: [
            './trafficLightSimulator.ts',
            './view/index.tsx'
        ]
    },

    output: {
        path: outPath,
        filename: "[name].js"
    },

    resolve: {
        extensions: [".ts", ".tsx", ".js", ".json"]
    },

    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: [/node_modules/, '/(src|build)/cli.ts', '/__tests__/'],
                loaders:  isProduction
                    ? 'awesome-typescript-loader'
                    : [
                        'react-hot-loader',
                        'awesome-typescript-loader'
                    ]
            },
            {
                test: /\.less/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            query: {
                                modules: true,
                                sourceMap: !isProduction,
                                importLoaders: 1,
                                localIdentName: '[local]__[hash:base64:5]'
                            }
                        },
                        {
                            loader: 'less-loader'
                        }
                    ]
                })
            },
            { test: /\.html$/, use: 'html-loader' },
        ]
    },

    plugins: [
        new webpack.optimize.AggressiveMergingPlugin(),
        new ExtractTextPlugin({
            filename: 'styles.css',
            disable: !isProduction
        }),
        new HtmlWebpackPlugin({
            template: 'index.html'
        })
    ],
    devServer: {
        contentBase: sourcePath,
        hot: true,
        stats: {
            warnings: false
        },
    },
    node: {
        fs: 'empty',
        net: 'empty'
    }
};